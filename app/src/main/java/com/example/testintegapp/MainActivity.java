package com.example.testintegapp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import in.juspay.godel.PaymentActivity;
import in.juspay.godel.data.JuspayResponseHandler;
import in.juspay.godel.ui.HyperPaymentsCallback;
import in.juspay.godel.ui.HyperPaymentsCallbackAdapter;
import in.juspay.godel.ui.JuspayWebView;
import in.juspay.services.PaymentServices;

public class MainActivity extends AppCompatActivity {

    private final String ACTION = "initiate";
    private final String ENVIRONMENT = "sandbox";
    private final String SERVICE = "in.juspay.hyperpay";
    private String clientId;
    private String merchantKeyId;
    private String merchantId;
    private String customerId;
    private String emailId;
    private String mobileNumber;
    private Button prefetch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }

        prefetch = findViewById(R.id.prefetch);

        prefetch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject payload =  prefetch();
                PaymentServices paymentServices = new PaymentServices( MainActivity.this);
                PaymentActivity.preFetch(MainActivity.this, clientId, false);
                Log.i("astast after prefetch :",payload.toString());
                Log.i("astast clientId :",clientId);
                paymentServices.initiate(payload, new HyperPaymentsCallbackAdapter() {
                    @Override
                    public void onEvent(JSONObject jsonObject, JuspayResponseHandler juspayResponseHandler) {
                        try {
                            String event = jsonObject.getString("event");
                            if(event.equals("show_loader")) {
                                // Show some loader here
                                Log.i("astast","Show loader");
                            } else if(event.equals("hide_loader")) {
                                // Hide Loader
                                Log.i("astast","Hide loader");
                            } else if(event.equals("initiate_result")){
                                // Get the response
                                JSONObject response = jsonObject.optJSONObject("payload");
                                Log.i("astast : Initiate resp",response.toString());
                            } else if(event.equals("process_result")){
                                // Get the response
                                JSONObject response = jsonObject.optJSONObject("payload");
                                Log.i("astast : Process resp",response.toString());
                            }
                            // merchant code...
                        } catch (Exception e) {
                            // merchant code...
                            Log.e("astast", "Exception in payments callback function");
                        }
                    }
                });
            }
        });

    }

    private JSONObject prefetch() {

        Toast.makeText(getApplicationContext(), "Clicked", Toast.LENGTH_SHORT).show();

        String timestamp, signaturePayload, signature;
        JSONObject initiateSDKPayload, initiateSDKRequest = null;

        clientId = ((EditText) findViewById(R.id.client_id)).getText().toString();
        merchantKeyId = ((EditText) findViewById(R.id.merchant_key_id)).getText().toString();
        merchantId = ((EditText) findViewById(R.id.merchant_id)).getText().toString();
        customerId = ((EditText) findViewById(R.id.customer_id)).getText().toString();
        emailId = ((EditText) findViewById(R.id.email_id)).getText().toString();
        mobileNumber = ((EditText) findViewById(R.id.mobile_number)).getText().toString();

        try {

            timestamp = Long.toString(System.currentTimeMillis());

            signaturePayload = (new JSONObject()).
                    put("first_name", "").
                    put("last_name", "").
                    put("merchant_id", merchantId).
                    put("customer_id", customerId).
                    put("mobile_number", mobileNumber).
                    put("email_address", emailId).
                    put("timestamp", timestamp).toString();


            signature = getSignature(signaturePayload);

            initiateSDKPayload = (new JSONObject()).
                    put("action", ACTION).
                    put("clientId", clientId).
                    put("merchantKeyId", merchantKeyId).
                    put("signaturePayload", signaturePayload).
                    put("signature", signature).
                    put("environment", ENVIRONMENT).
                    put("betaAssets", "false");


            initiateSDKRequest = (new JSONObject()).
                    put("requestId", timestamp).
                    put("service", SERVICE).
                    put("payload", initiateSDKPayload);

            Log.i("astast",initiateSDKRequest.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return initiateSDKRequest;
    }

    private String getSignature(String signaturePayload) throws InterruptedException, ExecutionException {

        class GetSignature extends AsyncTask<String, Integer, String> {

            protected String doInBackground(String... args) {
                try {
                    final URL url = new URL("http://dry-cliffs-89916.herokuapp.com/sign-hyper-beta?payload=" + args[0]);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestMethod("GET");
                    int responseCode = connection.getResponseCode();

                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        BufferedReader in = new BufferedReader(
                                new InputStreamReader(connection.getInputStream()));
                        String inputLine;
                        StringBuffer response = new StringBuffer();
                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }
                        in.close();
                        String signature = response.toString();
                        System.out.println("JSON String Result " + signature);
                        Log.i("astast : Signature ",signature);
                        return signature;
                    } else {
                        Log.e("astast", "Failed while creating signature, respCode : " + responseCode);
                        return "";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return "";
                }
            }
        }
        return new GetSignature().execute(signaturePayload).get();
    }
}
